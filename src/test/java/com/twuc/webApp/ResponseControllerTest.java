package com.twuc.webApp;

import com.twuc.webApp.model.Message;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class ResponseControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Test
    void response_status_is_200_when_method_is_empty() throws Exception {
        mockMvc.perform(get("/api/no-return-value"))
                .andExpect(status().isOk());
    }

    @Test
    void should_return_204_with_annotation() throws Exception {
        mockMvc.perform(get("/api/no-return-value-with-annotation"))
                .andExpect(status().is(204));
    }

    @Test
    void should_return_string_and_content_type_is_text() throws Exception {
        mockMvc.perform(get("/api/messages/helloworld"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(content().string("helloworld"));
    }

    @Test
    void should_return_message_instance_and_value_is_message() throws Exception {
        mockMvc.perform(get("/api/message-objects/one"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.value").value("one"));
    }

    @Test
    void should_return_Message_and_value_is_message_with_annotation() throws Exception {
        mockMvc.perform(get("/api/message-objects-with-annotation/hey"))
                .andExpect(status().is(202))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.value").value("hey"));
    }

    @Test
    void should_create_ResponseEntity() throws Exception {
        mockMvc.perform(get("/api/message-entities/entities"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(header().string("X-Auth", "me"))
                .andExpect(jsonPath("$.value").value("entities"));
    }
}
