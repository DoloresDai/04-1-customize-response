package com.twuc.webApp.controller;

import com.twuc.webApp.model.Message;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ResponseController {
    @GetMapping("/api/no-return-value")
    public void noResponse() {
    }

    @GetMapping("/api/no-return-value-with-annotation")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void responseNoContentWithAnnotation() {
    }

    @GetMapping("/api/messages/{message}")
    public String returnHelloworld(@PathVariable String message) {
        return message;
    }

    @GetMapping("/api/message-objects/{message}")
    public Message returnMessageInstance(@PathVariable String message) {
        return new Message(message);
    }

    @GetMapping("/api/message-objects-with-annotation/{message}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Message returnMessageInstanceWithAnnotation(@PathVariable String message) {
        return new Message(message);
    }

    @GetMapping("/api/message-entities/{message}")
    public ResponseEntity<Message> createResponseEntity(@PathVariable String message) {
        return ResponseEntity
                .ok()
                .header("X-Auth", "me")
                .body(new Message(message));
    }

}
